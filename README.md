nixy
---

NixOS Configuration.

To install it:
- `git clone https://gitlab.com/huskyhaskell/nixy`
- make sure proper dynamically generated `hardware-configuration.nix` exists
  within the cloned directory
- `sudo nixos-rebuild switch --flake .#schyer`
