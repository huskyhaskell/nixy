-- packer configuration {{{
local install_path = vim.fn.stdpath 'data' .. '/site/pack/packer/start/packer.nvim'

if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
  vim.fn.execute('!git clone https://github.com/wbthomason/packer.nvim ' .. install_path)
end

vim.api.nvim_exec(
  [[
  augroup Packer
    autocmd!
    autocmd BufWritePost init.lua PackerCompile
  augroup end
]],
  false
)
-- }}}

-- plugin declarations {{{
local use = require('packer').use
require('packer').startup(function()
  use 'wbthomason/packer.nvim' -- Package manager
  use 'nvim-treesitter/nvim-treesitter'
  use 'neovim/nvim-lspconfig' -- Collection of configurations for built-in LSP client
  use 'ishan9299/nvim-solarized-lua'
  use 'Olical/conjure'
  use 'wlangstroth/vim-racket'
  use {
    'nvim-telescope/telescope.nvim',
    requires = {
      {'nvim-lua/popup.nvim'},
      {'nvim-lua/plenary.nvim'}
    }
  }
  use 'kyazdani42/nvim-web-devicons'
  use 'hrsh7th/nvim-compe'
  use 'folke/which-key.nvim'
  use 'hoob3rt/lualine.nvim'
  use 'kristijanhusak/orgmode.nvim'
  use {
    'TimUntersberger/neogit',
    requires = 'nvim-lua/plenary.nvim'
  }
  use {
    'lewis6991/gitsigns.nvim',
    requires = 'nvim-lua/plenary.nvim'
  }
  use 'phaazon/hop.nvim'
  use 'LnL7/vim-nix'
end)
-- }}}

-- options {{{
--
-- Set completeopt to have a better completion experience
vim.o.completeopt = 'menuone,noselect'
--Incremental live completion
vim.o.inccommand = 'nosplit'

-- fold method
vim.o.foldmethod = 'marker'

vim.api.nvim_set_keymap('', '<Space>', '<Nop>', { noremap = true, silent = true })
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

-- Expand tabs into spaces
vim.o.expandtab = true
vim.o.shiftwidth = 4
vim.o.tabstop = 4
vim.o.softtabstop = 4

-- No line wrap
vim.wo.wrap = false

--Set highlight on search
vim.o.hlsearch = false

--Make line numbers default
vim.wo.number = true

--Do not save when switching buffers
vim.o.hidden = true

--Enable break indent
vim.o.breakindent = true

--Save undo history
vim.cmd [[set undofile]]

--Case insensitive searching UNLESS /C or capital in search
vim.o.ignorecase = true
vim.o.smartcase = true

-- Change preview window location
vim.g.splitbelow = true

--Decrease update time
vim.o.updatetime = 250
vim.wo.signcolumn = 'yes'
-- }}}

-- colorscheme {{{
vim.o.termguicolors = true
vim.cmd[[colorscheme solarized]]
vim.g.background = 'light'
-- }}}

-- terminal configuration {{{
--Remap escape to leave terminal mode
vim.api.nvim_set_keymap('t', '<Esc>', [[<c-\><c-n>]], { noremap = true })

--Disable numbers in terminal mode
vim.api.nvim_exec(
  [[
  augroup Terminal
    autocmd!
    au TermOpen * set nonu
  augroup end
]],
  false
)
-- }}}

-- treesitter configuration {{{
local parser_configs = require('nvim-treesitter.parsers').get_parser_configs()

require('nvim-treesitter.configs').setup {
  ensure_installed = "all",
  highlight = {
    enable = true, -- false will disable the whole extension
  },
  indent = {
    enable = true,
  },
}
-- }}}

-- telescope configuration {{{
require('telescope').setup()
vim.api.nvim_set_keymap('n', '<Leader><Leader>', [[<cmd>Telescope builtin<CR>]], { noremap = true })
vim.api.nvim_set_keymap('n', '<Leader>ff', [[<cmd>Telescope find_files<CR>]], { noremap = true })
vim.api.nvim_set_keymap('n', '<Leader>fg', [[<cmd>Telescope live_grep<CR>]], { noremap = true })
vim.api.nvim_set_keymap('n', '<Leader>fb', [[<cmd>Telescope buffers<CR>]], { noremap = true })
vim.api.nvim_set_keymap('n', '<Leader>fh', [[<cmd>Telescope help_tags<CR>]], { noremap = true })
vim.api.nvim_set_keymap('n', '<Leader>fs', [[<cmd>Telescope current_buffer_fuzzy_find<CR>]], { noremap = true })
-- }}}

-- statusline configuration {{{
require('lualine').setup({
  options = {
    theme = "solarized",
    section_separators = '',
    component_separators = '',
  },
  sections = {
    lualine_a = { "mode" },
    lualine_b = { "branch" },
    lualine_c = { { "diagnostics", sources = { "nvim_lsp" } }, "filename" },
    lualine_x = { "filetype", lsp_progress },
    lualine_y = { "progress" },
    lualine_z = { clock },
  },
})
-- }}}

-- lsp configuration {{{
local nvim_lsp = require('lspconfig')
local on_attach = function(_client, bufnr)
  vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')
  vim.lsp.handlers['textDocument/publishDiagnostics'] = vim.lsp.with(vim.lsp.diagnostic.on_publish_diagnostics, {
    virtual_text = true,
    signs = true,
    update_in_insert = false,
  })

  local overridden_hover = vim.lsp.handlers.hover
  vim.lsp.handlers['textDocument/hover'] = function(...)
    local buf = overridden_hover(...)
    vim.api.nvim_buf_set_keymap(buf, 'n', 'K', '<Cmd>wincmd p<CR>', { noremap = true, silent = true })
  end

  -- Mappings.
  local opts = { noremap = true, silent = true }
  vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gD', '<Cmd>lua vim.lsp.buf.declaration()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gd', '<Cmd>lua vim.lsp.buf.definition()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', 'K', '<Cmd>lua vim.lsp.buf.hover()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>ca', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>e', '<cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '[d', '<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', ']d', '<cmd>lua vim.lsp.diagnostic.goto_next()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>q', '<cmd>lua vim.lsp.diagnostic.set_loclist()<CR>', opts)
  FormatRange = function()
    local start_pos = vim.api.nvim_buf_get_mark(0, '<')
    local end_pos = vim.api.nvim_buf_get_mark(0, '>')
    vim.lsp.buf.range_formatting({}, start_pos, end_pos)
  end

  vim.cmd [[
    command! -range FormatRange  execute 'lua FormatRange()'
  ]]

  vim.cmd [[
    command! Format execute 'lua vim.lsp.buf.formatting()'
  ]]

end

local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.completion.completionItem.snippetSupport = true

local servers = {
  'clangd',
  'texlab',
}
for _, lsp in ipairs(servers) do
  nvim_lsp[lsp].setup {
    capabilities = capabilities,
    on_attach = on_attach,
  }
end

-- }}}

-- nvim-compe configuration {{{
require'compe'.setup {
  enabled = true,
  autocomplete = true,
  debug = false,
  min_length = 1,
  preselect = 'enable',
  throttle_time = 80,
  source_timeout = 200,
  resolve_timeout = 800,
  incomplete_delay = 400,
  max_abbr_width = 100,
  max_kind_width = 100,
  max_menu_width = 100,
  documentation = {
    border = { '', '' ,'', ' ', '', '', '', ' ' }, -- the border option is the same as `|help nvim_open_win|`
    winhighlight = "NormalFloat:CompeDocumentation,FloatBorder:CompeDocumentationBorder",
    max_width = 120,
    min_width = 60,
    max_height = math.floor(vim.o.lines * 0.3),
    min_height = 1,
  },

  source = {
    path = true,
    buffer = true,
    calc = true,
    nvim_lsp = true,
    -- luasnip = true;
  };
}

vim.api.nvim_set_keymap('i', '<C-Space>', [[compe#complete()]], { noremap = true, expr = true, silent = true })
vim.api.nvim_set_keymap('i', '<CR>', [[compe#confirm('<CR>')]], { noremap = true, expr = true, silent = true })
-- }}}

-- which-key configuration {{{
require('which-key').setup{
  window = {
    border = { '─', '─', '─', ' ', ' ', ' ', ' ', ' ' }, -- none, single, double, shadow
    position = 'bottom', -- bottom, top
    margin = { 0, 0, 0, 0 }, -- extra window margin [top, right, bottom, left]
    padding = { 0, 0, 1, 0 }, -- extra window padding [top, right, bottom, left]
  },
}
-- }}}

-- nvim-web-devicons configuration {{{
require('nvim-web-devicons').setup()
-- }}}

-- neogit configuration {{{
local neogit = require('neogit')

neogit.setup()
-- }}}

-- gitsigns configuration {{{
local gitsigns = require('gitsigns')

gitsigns.setup()
-- }}}

-- hop configuration {{{
require('hop').setup()
vim.api.nvim_set_keymap('n', 'gsw', [[<cmd>HopWord<CR>]], {})
vim.api.nvim_set_keymap('n', 'gs/', [[<cmd>HopPattern<CR>]], {})
vim.api.nvim_set_keymap('n', 'gsc', [[<cmd>HopChar1<CR>]], {})
vim.api.nvim_set_keymap('n', 'gsC', [[<cmd>HopChar2<CR>]], {})
vim.api.nvim_set_keymap('n', 'gsl', [[<cmd>HopLine<CR>]], {})
-- }}}

-- orgmode.nvim configuration {{{
require('orgmode').setup()
-- }}}

-- misc {{{
-- Highlight on yank
vim.api.nvim_exec(
  [[
  augroup YankHighlight
    autocmd!
    autocmd TextYankPost * silent! lua vim.highlight.on_yank()
  augroup end
]],
  false
)
-- }}}
