{ config, pkgs, ... }:
let
  myGnomeExtensions = with pkgs.gnomeExtensions; [
    just-perfection
    blur-my-shell
    espresso
  ];
in
{
  imports =
    [
      ./hardware-configuration.nix
    ];

  nix = {
    package = pkgs.nixUnstable;
    trustedUsers = [ "root" "david" ];
    extraOptions = ''
      experimental-features = nix-commmand flakes
    '';
    binaryCaches = [
      "https://cachix.cachix.org"
      "https://nix-community.cachix.org"
    ];
    binaryCachePublicKeys = [
      "cachix.cachix.org-1:eWNHQldwUO7G2VkjpnjDbWwy4KQ/HNxht7H4SSoMckM="
      "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
    ];
  };

  boot.loader = {
    systemd-boot.enable = true;
    efi.canTouchEfiVariables = true;
  };

  time.timeZone = "Europe/Stockholm";

  networking = {
    hostName = "schyer";
    networkmanager.enable = true;
    useDHCP = false;
    interfaces = {
      enp57s0f1.useDHCP = true;
      wlp58s0.useDHCP = true;
    };
  };

  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };

  services.xserver = {
    enable = true;
    displayManager.gdm.enable = true;
    desktopManager.gnome = {
      enable = true;
    };
    layout = "us";
    xkbOptions = "caps:swapescape";
    libinput.enable = true;
  };

  hardware = {
    pulseaudio.enable = false;
    opengl.driSupport32Bit = true;
  };

  security.rtkit.enable = true;

  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
  };

  programs.zsh.enable = true;

  users.users.david = {
    isNormalUser = true;
    extraGroups = [ "wheel" ];
    shell = pkgs.zsh;
  };

  environment.systemPackages = with pkgs; [
    vim
    wget
    chromium
    cachix
    coreutils
  ] ++ myGnomeExtensions;

  fonts.fonts = with pkgs; [
    (nerdfonts.override { fonts = [ "JetBrainsMono" ]; })
  ];

  system.stateVersion = "21.05";
}
