{ config, options, lib, pkgs, ... }:
with lib;
let
  cfg = config.modules.tools.nix;
in
{
  options.modules.tools.nix = {
    enable = mkOption {
      type = types.bool;
      default = false;
      description = ''
        Enable some nix tools, such as direnv and lorri.
      '';
    };
  };

  config = mkIf cfg.enable {
    programs.direnv = {
      enable = true;
      enableZshIntegration = true;
    };
    services.lorri.enable = true;
  };
}
