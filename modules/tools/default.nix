{ config, pkgs, ... }:
{
  imports = [
    ./cli.nix
    ./terminal.nix
    ./nix.nix
  ];
}
