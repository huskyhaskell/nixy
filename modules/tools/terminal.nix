{ config, options, lib, pkgs, ...}:
with lib;
let
  cfg = config.modules.tools.terminal;
in
{
  options.modules.tools.terminal = {
    enable = mkOption {
      type = types.bool;
      default = false;
      description = ''
        Enable the kitty terminal emulator.
      '';
    };
  };

  config = mkIf cfg.enable {
    programs.kitty = {
      enable = true;
      font.name = "JetBrainsMono Nerd Font";
      font.size = 14;
      extraConfig = ''
        background #fdf6e3
        foreground #52676f
        cursor #52676f
        selection_background #e9e2cb
        color0 #e4e4e4
        color8 #ffffd7
        color1 #d70000
        color9 #d75f00
        color2 #5f8700
        color10 #585858
        color3 #af8700
        color11 #626262
        color4 #0087ff
        color12 #808080
        color5 #af005f
        color13 #5f5faf
        color6 #00afaf
        color14 #8a8a8a
        color7 #262626
        color15 #1c1c1c
        selection_foreground #fcf4dc
      '';
      settings = {
        clear_all_shortcuts = "yes";
        disable_ligatures = "always";
        kitty_mod = "ctrl+shift";
      };
      keybindings = {
        # Scrolling
        "kitty_mod+j" = "scroll_line_down";
        "kitty_mod+k" = "scroll_line_up";

        # Window Behaviour
        "kitty_mod+n" = "new_os_window";
        "kitty_mod+w" = "close_window";
        "kitty_mod+1" = "first_window";
        "kitty_mod+2" = "second_window";
        "kitty_mod+3" = "third_window";
        "kitty_mod+4" = "fourth_window";
        "kitty_mod+5" = "fifth_window";
        "kitty_mod+6" = "sixth_window";
        "kitty_mod+7" = "seventh_window";
        "kitty_mod+8" = "eighth_window";
        "kitty_mod+9" = "ninth_window";
        "kitty_mod+0" = "tenth_window";

        # Tab Behaviour
        "kitty_mod+t"     = "new_tab";
        "kitty_mod+q"     = "close_tab";
        "kitty_mod+."     = "move_tab_forward";
        "kitty_mod+,"     = "move_tab_backward";
        "kitty_mod+alt+t" = "set_tab_title";

        # Layout Behaviour
        "alt+l" = "next_layout";
      };
    };
  };
}
