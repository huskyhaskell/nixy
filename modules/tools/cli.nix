{ config, options, lib, pkgs, ... }:
with lib;
let
  cfg = config.modules.tools.cli;
in
{
  options.modules.tools.cli = {
    enable = mkOption {
      type = types.bool;
      default = false;
      description = ''
        Enable the most common cli tools, including zsh as the interactive shell.
      '';
    };
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      exa
      bat
      fd
      ripgrep
      tokei
      bitwarden-cli
      fzf
      procs
      neofetch
      htop
    ];

    programs.starship = {
      enable = true;
      settings = {
        add_newline = true;
        character = {
          success_symbol = "[λ](bold yellow)";
          error_symbol = "[λ](bold red)";
        };
      };
    };

    programs.zsh = {
      enable = true;
      enableCompletion = true;
      initExtraFirst = ''
        if [ -d "$HOME/.emacs.d/bin" ]; then
        export PATH="$HOME/.emacs.d/bin":"$PATH"
        fi
      '';
      shellAliases = {
        "ls" = "exa";
        "cat" = "bat --theme=OneHalfDark";
        "v" = "nvim";
      };
      plugins = [
        {
          name = "zsh-nix-shell";
          file = "nix-shell.plugin.zsh";
          src = pkgs.fetchFromGitHub {
            owner = "chisui";
            repo = "zsh-nix-shell";
            rev = "v0.2.0";
            sha256 = "1gfyrgn23zpwv1vj37gf28hf5z0ka0w5qm6286a7qixwv7ijnrx9";
          };
        }
      ];
    };

    programs.git = {
      enable = true;
      userEmail = "david.holmqvist@mailbox.org";
      userName = "David Holmqvist";
    };
  };
}
