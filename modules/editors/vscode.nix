{ config, options, lib, pkgs, ... }:
with lib;
let
  cfg = config.modules.editors.vscode;
in
{
  options.modules.editors.vscode = {
    enable = mkOption {
      type = types.bool;
      default = false;
      description = ''
        Enable the vscode editor with some extensions, most
        nostably vsliveshare for sharing programming sessions.
      '';
    };
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      shellcheck
    ];

    programs.vscode = {
      enable = true;
      package = pkgs.vscode;
      extensions = with pkgs.vscode-extensions; [
        ms-vsliveshare.vsliveshare
        ms-vscode.cpptools
        vscodevim.vim
        timonwong.shellcheck
      ] ++ (pkgs.vscode-utils.extensionsFromVscodeMarketplace [
        {
          name = "language-x86-64-assembly";
          publisher = "13xforever";
          version = "3.0.0";
          sha256 = "c08b18e85ba1b3aefa107f2b4b3e1f4c77a656139ee527bd498dd0f6202aaf53";
        }
        {
          name = "solarized";
          publisher = "ryanolsonx";
          version = "2.0.3";
          sha256 = "3099ca2eda34717895a97073adb990a21fec41d26b918993a5f879afc7213f1c";
        }
      ]);
      userSettings = {
        "extensions.autoUpdate" = false;
        "extensions.autoCheckUpdates" = false;
        "workbench.colorTheme" = "Solarized Light+";
        "editor.fontFamily" = "JetBrainsMono Nerd Font";
        "editor.fontSize" = 16;
        "terminal.integrated.fontFamily" = "JetBrainsMono Nerd Font";
        "terminal.integrated.fontSize" = 15;
        "keyboard.dispatch" = "keyCode";
      };
    };
  };
}
