{ config, lib, pkgs, ...}:
with lib;
let
  cfg = config.modules.editors.emacs;
in
{
  options.modules.editors.emacs = {
    enable = mkOption {
      type = types.bool;
      default = false;
      description = ''
        Enable emacs, running on pgtk, with native compilation.
      '';
    };
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      graphviz # for org-roam-graph
      gcc # for emacsql
      texlive.combined.scheme-full # org & latex
      gnutls # irc
      shellcheck
    ];

    programs.emacs = {
      enable = true;
      package = pkgs.emacsPgtkGcc;
      extraPackages = epkgs: with epkgs; [
        vterm
      ];
    };
  };
}
