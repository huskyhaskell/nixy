{ options, config, lib, pkgs, ...}:
with lib;
let
  cfg = config.modules.editors.neovim;
in
{
  options.modules.editors.neovim = {
    enable = mkOption {
      type = types.bool;
      default = false;
      description = ''
        Enable the neovim editor.
      '';
    };
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      gcc
      clang-tools
      texlab
      texlive.combined.scheme-full
    ];

    programs.neovim = {
      enable = true;
      package = pkgs.neovim-nightly;
    };

    xdg.configFile."nvim/init.lua".source = ../../configs/neovim/init.lua;
  };
}
