{ config, options, lib, pkgs, ...}:
with lib;
let
  cfg = config.modules.desktop;
in
{
  options.modules.desktop = {
    enable = mkOption {
      type = types.bool;
      default = false;
      description = ''
        Enable common desktop applications.
      '';
    };
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      bitwarden
      chromium
      discord
      element-desktop
      nyxt
      spotify
      lutris
      torbrowser
    ];
  };
}
