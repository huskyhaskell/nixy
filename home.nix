{ pkgs, ... }:

{
  programs.home-manager.enable = true;

  home = {
    username = "david";
    homeDirectory = "/home/david";
    stateVersion = "20.09";
  };

  imports = [
    ./modules
  ];

  modules = {
    desktop.enable = true;
    editors = {
      emacs.enable = true;
      neovim.enable = true;
      vscode.enable = true;
    };
    tools = {
      cli.enable = true;
      terminal.enable = true;
      nix.enable = true;
    };
  };
}
