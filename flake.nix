{
  description = "Flake for managing NixOS systems";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    neovim-nightly-overlay.url = "github:nix-community/neovim-nightly-overlay";
    emacs-overlay.url = "github:nix-community/emacs-overlay";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = inputs@{ self, ... }:
    let
      overlays = with inputs; [
        emacs-overlay.overlay
        neovim-nightly-overlay.overlay
      ];
    in
    {
    nixosConfigurations = {
      schyer = inputs.nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          ./configuration.nix

          inputs.home-manager.nixosModules.home-manager
          {
            nixpkgs = {
              inherit overlays;
              config.allowUnfree = true;
            };
            home-manager = {
              useGlobalPkgs = true;
              useUserPackages = true;
              users.david = import ./home.nix {
                inherit inputs;
                pkgs = inputs.nixpkgs;
              };
            };
          }
        ];
      };
    };
  };
}
